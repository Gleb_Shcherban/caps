import React from 'react';
import icon from './images/SV Blue.png';

export const SocialVenuIcon: React.FC = () => <img src={icon} alt="" style={{ height: '24px', width: 'auto' }} />;
