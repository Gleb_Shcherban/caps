import React from 'react';
import icon from './images/Melin logo.png';

export const MelinLogo: React.FC = () => <img src={icon} alt="" style={{ height: '33px', width: 'auto', zIndex: 10, }} />;
