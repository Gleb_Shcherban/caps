export enum UploaVideoStrings {
  ConfirmEmail = 'Confirm your email to access reward',
}

export enum EmailPopupStrings {
  TitleFirstText = 'Become a ',
  TitleSecondText = 'Melin Ambassador',
  BenefitsListEarn = '— Earn 10% off by creating a video',
  BenefitsListOffers = '— Get exclusive offers',
  BenefitsListOffersFreeItems = '— Win free items',
  PrivacyPolicyText1 = 'Agree to SocialVenu ',
  PrivacyPolicyText2 = 'terms & conditions',
}

export enum SideBarStrings {
  Home = 'Home',
  EmailUs = 'Email Us',
  LiveChat = 'Live Chat',
}

export enum ReferencesPageStrings {
  TitleFirstText = 'Choose your',
  TitleSecondText = 'Story',
}

export enum BottomBarStrings {
  PoweredBySocialVenu = 'Powered by SocialVenu',
  Terms = 'Terms of Use',
}

export enum StoryPageStrings {
  TitleFirstText = 'Upload your',
  TitleSecondText = 'Video',
  ChangeStory = 'Change Story',
  UploadButton = 'Select from your camera roll',
  ExplanationAboutContent = 'Where does my content go?',
}
