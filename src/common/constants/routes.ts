export enum AppRoutes {
  Home = '/',
  EmailUs = '/email-us',
  LiveChat = '/live-chat',
  UploadVideo = '/upload-video',
  Story = '/story',
  References = '/references',
  PrivacyPolicy = '/terms-of-use-privacy-policy',
}
