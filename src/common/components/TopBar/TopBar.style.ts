import styled from 'styled-components';

export const TopBarContainer = styled.div`
  display: flex;
  width: 100%;
  height: 53px;
  justify-content: space-between;
  padding-bottom: 10px;
`;
