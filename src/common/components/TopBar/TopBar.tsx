import React, { useState } from 'react';
import { TopBarContainer } from './TopBar.style';
import { SmallSquareButton, SmallSquareButtonType } from '../SmallSquareButton/SmallSquareButton';
import { MelinLogo } from '../../assets/MelinLogo';

export const TopBar: React.FC = () => {
  const [sideBarIsOpen, setSideBarIsOpen] = useState(false);

  const onOpenSideBarButtonClick = () => setSideBarIsOpen(!sideBarIsOpen);
  return (
    <TopBarContainer>
      <SmallSquareButton
        onButtonClick={() => onOpenSideBarButtonClick()}
        buttonType={sideBarIsOpen ? SmallSquareButtonType.Close : SmallSquareButtonType.Hamburger}
      />
      <MelinLogo />
    </TopBarContainer>
  );
};
