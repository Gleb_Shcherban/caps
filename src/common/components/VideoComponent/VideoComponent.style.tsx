import styled from 'styled-components';

export const ImgWrapper = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

export const VideoSliderWrapper = styled.div`
  position: relative;
  height: 190px;

  div {
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  img {
    width: 40px;
    height: 40px;
  }

  video {
    width: 100%;
    height: 100%;
  }
`;

export const VideoExampleWrapper = styled.div`
  position: relative;
  margin-right: 10px;
  height: 161px;
  min-width: 97px;
  border-radius: var(--commonBorderRadius);
  overflow: hidden;
  background-color: #222;

  :focus {
    outline: none;
  }

  div {
    margin: auto;
    display: flex;
    align-items: flex-end;
    padding: 10px;
  }

  img {
    width: 30px;
    height: 30px;
  }

  video {
    width: 100%;
    height: 100%;
  }
`;
