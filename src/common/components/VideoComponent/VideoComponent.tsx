import React, { useRef, useState } from 'react';

import PlayVideoImage from '../../assets/images/PlayVideoImage.png';

import { ImgWrapper } from './VideoComponent.style';

interface VideoComponentProps {
  url: string;
}

export const VideoComponent: React.FC<VideoComponentProps> = (props) => {
  const { url } = props;
  const refVideo = useRef<HTMLVideoElement | null>(null);

  const [isVideoPlayButton, setIsVideoPlayButton] = useState(true);

  const onPlayVideoHandler = () => {
    const videoElement = refVideo.current as HTMLVideoElement;
    videoElement
      .play()
      .then(() => {
        setIsVideoPlayButton(false);
      })
      .catch((error) => {
        throw error;
      });
  };

  const handleTap = (event: React.MouseEvent<HTMLVideoElement>) => {
    const videoElement = refVideo.current as HTMLVideoElement;
    const target = event.target as HTMLVideoElement;

    if (videoElement.paused) {
      target.play().catch((error) => {
        throw error;
      });
    } else {
      target.pause();
    }
  };

  return (
    <>
      <video ref={refVideo} onClick={handleTap} preload="auto" src={url} playsInline loop />
      {isVideoPlayButton && (
        <ImgWrapper onClick={onPlayVideoHandler}>
          <img src={PlayVideoImage} alt="play-video-button" />
        </ImgWrapper>
      )}
    </>
  );
};
