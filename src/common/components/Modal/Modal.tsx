import React from 'react';

import { CloseIconWrapper, Content, ModalOverlay } from './Modal.style';
import { SmallSquareButton, SmallSquareButtonType } from '../SmallSquareButton/SmallSquareButton';

interface ModalProps {
  onClose: () => void;
  children: React.ReactNode;
}

export const Modal: React.FC<ModalProps> = ({ onClose, children }) => {
  const onClickContentHandler = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
  };

  return (
    <ModalOverlay onClick={onClose}>
      <Content onClick={onClickContentHandler}>
        <CloseIconWrapper>
          <SmallSquareButton buttonType={SmallSquareButtonType.Close} onButtonClick={onClose} />
        </CloseIconWrapper>
        {children}
      </Content>
    </ModalOverlay>
  );
};
