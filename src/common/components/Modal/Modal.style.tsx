import styled from 'styled-components';

export const ModalOverlay = styled.div`
  position: fixed;
  background-color: rgba(0, 0, 0, 0.8);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  z-index: 10;
`;

export const Content = styled.div`
  position: relative;
  margin-top: 84px;
  min-width: 320px;
  width: 100%;
  height: fit-content;
`;

export const CloseIconWrapper = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
  cursor: pointer;
`;
