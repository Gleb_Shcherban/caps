import React from 'react';

import { TitleText } from './Title.style';

interface TitleProps {
  firstText: string;
  secondaryText: string;
  noPadding?: boolean;
}

export const Title: React.FC<TitleProps> = ({ firstText, secondaryText, noPadding }) => {
  return (
    <TitleText addPadding={!noPadding}>
      <span>{firstText}</span>
      <br />
      <span>{secondaryText}</span>
    </TitleText>
  );
};
