import styled from 'styled-components';

interface TitleTextProps {
  addPadding?: boolean;
}
export const TitleText = styled.h1<TitleTextProps>`
  font-size: 28px;
  ${(props) => props.addPadding && 'padding: var(--titlePadding);'}

  span:first-child {
    font-weight: 400;
    color: var(--white);
  }

  span:nth-child(n + 2) {
    font-weight: 800;
    color: var(--blueTextColor);
  }
`;
