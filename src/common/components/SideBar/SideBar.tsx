import React from 'react';
import { RouteButton, RoutesButtons, SideBarContainer } from './SideBar.style';
import { TopBar } from '../TopBar/TopBar';
import { SideBarOptions } from './SideBarOptions';
import { AppRoutes } from '../../constants/routes';
import { BottomBar } from '../SocialVenuBottomBar/BottomBar';

export const Sidebar: React.FC = () => {
  const goToRoute = (url: AppRoutes) => {
    console.log('Go TO Next Route', url);
  };
  // TODO: decide on router implementation and use current route to define active tab
  const renderRoutesButtons = () => {
    return SideBarOptions.map((sidebarOption) => {
      return (
        <RouteButton
          key={sidebarOption.url}
          isActive={sidebarOption.url === AppRoutes.Home}
          onClick={() => goToRoute(sidebarOption.url)}
        >
          {sidebarOption.title}
        </RouteButton>
      );
    });
  };
  return (
    <SideBarContainer>
      <TopBar />
      <RoutesButtons>{renderRoutesButtons()}</RoutesButtons>
      <BottomBar />
    </SideBarContainer>
  );
};
