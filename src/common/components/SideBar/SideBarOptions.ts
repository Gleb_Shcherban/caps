import { AppRoutes } from '../../constants/routes';
import { SideBarStrings } from '../../localization/en';

interface SideBarOption {
  title: string;
  url: AppRoutes;
}

export const SideBarOptions: SideBarOption[] = [
  {
    title: SideBarStrings.Home,
    url: AppRoutes.Home,
  },
  {
    title: SideBarStrings.EmailUs,
    url: AppRoutes.EmailUs,
  },
  {
    title: SideBarStrings.LiveChat,
    url: AppRoutes.LiveChat,
  },
];
