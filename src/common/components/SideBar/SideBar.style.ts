import styled from 'styled-components';

export const SideBarContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  padding: var(--pagePadding);
  flex-direction: column;
`;

export const RoutesButtons = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  padding: 10px;

  & > div:last-child {
    margin-bottom: 40px;
  }

  &:after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 10px;
    height: 1px;
    width: 40px;
    border: 1px solid white;
  }
`;

interface RouteButtonProps {
  isActive: boolean;
}

export const RouteButton = styled.div<RouteButtonProps>`
  font-size: 30px;
  line-height: 70px;
  font-weight: 300;

  ${(props) =>
    props.isActive &&
    `
      font-weight: 800;
      color: blue;
  `}
`;
