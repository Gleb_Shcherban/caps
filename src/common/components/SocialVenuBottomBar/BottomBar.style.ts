import styled from 'styled-components';

export const BottomBarContainer = styled.div`
  margin-top: auto;
  display: flex;
  align-items: center;
  height: 24px;
  width: 100%;
  padding: 10px;
`;

export const BottomBarText = styled.div`
  font-size: 10px;
  font-weight: 300;
  margin-left: 12px;
`;

export const TermsButton = styled(BottomBarText)`
  margin-left: auto;
`;
