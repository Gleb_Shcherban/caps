import React from 'react';
import { BottomBarContainer, BottomBarText, TermsButton } from './BottomBar.style';
import { SocialVenuIcon } from '../../assets/SocialVenu';
import { BottomBarStrings } from '../../localization/en';

export const BottomBar: React.FC = () => (
  <BottomBarContainer>
    <SocialVenuIcon />
    <BottomBarText>{BottomBarStrings.PoweredBySocialVenu}</BottomBarText>
    <TermsButton>{BottomBarStrings.Terms}</TermsButton>
  </BottomBarContainer>
);
