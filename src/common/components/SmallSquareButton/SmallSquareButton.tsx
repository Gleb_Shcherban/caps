import React from 'react';
import { Rectangle } from './SmallSquareButton.style';
import { Hamburger } from '../../assets/Hamburger';
import { CloseIcon } from '../../assets/CloseIcon';

export enum SmallSquareButtonType {
  Close,
  Hamburger,
}

interface SmallSquareButtonProps {
  onButtonClick: () => void;
  buttonType: SmallSquareButtonType;
}

export const SmallSquareButton: React.FC<SmallSquareButtonProps> = ({ onButtonClick, buttonType }) => {
  let icon;
  switch (buttonType) {
    case SmallSquareButtonType.Close:
      icon = <CloseIcon />;
      break;
    case SmallSquareButtonType.Hamburger:
      icon = <Hamburger />;
      break;
  }
  return <Rectangle onClick={() => onButtonClick()}>{icon}</Rectangle>;
};
