import styled from 'styled-components';

export const Rectangle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 33px;
  height: 33px;
  background-color: var(--darkGreyButtonColor);
  border-radius: var(--buttonBorderRadius);
`;
