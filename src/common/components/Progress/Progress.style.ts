import styled from "styled-components";

export const ProgressWrapper = styled.div`
  width: 100%;
  height: 5px;
  border-radius: 14px;
  display: inline-block;
  background-color: #fff;
  overflow-x: hidden;
  position: relative;
`;

interface ProgressLineProps {
  now: number
}

export const ProgressLine = styled.div<ProgressLineProps>`
  display: inline-block;
  position: absolute;
  width: inherit;
  height: inherit;
  border-radius: 14px;
  background-color: crimson;
  background-image: var(--primaryBgrGradientHorizontal);
  transition: all .3s ease-in-out;
  ${props =>
    `transform: translateX(${-100 + props.now}%);`
  }
`;

