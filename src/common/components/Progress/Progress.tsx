import React from 'react';
import { ProgressLine, ProgressWrapper } from './Progress.style';

interface ProgressProps {
  now: number
}

export const Progress: React.FC<ProgressProps> = ({
  now
}) => {
  return (
    <ProgressWrapper>
      <ProgressLine now={now} />
    </ProgressWrapper>
  )
}
