import React from 'react';

import { Container, InputField, SubmitButton } from './InputWithButtonField.style';

interface InputWithButtonFieldProps {
  buttonText: string;
  value: string;
  placeholder: string;
  onChange: (value: string) => void;
  onSubmit: () => void;
}

export const InputWithButtonField: React.FC<InputWithButtonFieldProps> = (props) => {
  const { buttonText, value = '', placeholder, onChange, onSubmit } = props;

  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <Container>
      <InputField autoComplete="off" value={value} onChange={onChangeHandler} placeholder={placeholder} />
      <SubmitButton onClick={onSubmit}>{buttonText}</SubmitButton>
    </Container>
  );
};
