import styled from 'styled-components';
import { Button } from '../../styles/commonStyles.style';

export const Container = styled.div`
  display: flex;
  height: 40px;
  border-radius: var(--buttonBorderRadius);
  overflow: hidden;
`;

export const InputField = styled.input`
  width: 100%;
  height: 100%;
  padding-left: 20px;
  font-weight: 400;
  color: var(--black);
  background-color: var(--white);
`;

export const SubmitButton = styled(Button)`
  width: 88px;
  height: 100%;
  border-radius: 0;
`;
