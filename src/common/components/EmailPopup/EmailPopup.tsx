import React, { useMemo, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { AppRoutes } from '../../constants/routes';

import { EmailPopupStrings } from '../../localization/en';

import { mainWrapperScrollManager } from '../../../services/utilities';

import { Modal } from '../Modal/Modal';
import { Title } from '../Title/Title';
import { InputWithButtonField } from '../InputWithButtonField/InputWithButtonField';

import TopImageMock from './Bitmap.png';

import { ModalContent, TopImage, PromoteText, BenefitsList, BenefitsItem, PrivacyPolicy } from './EmailPopup.style';

const benefits = [
  EmailPopupStrings.BenefitsListEarn,
  EmailPopupStrings.BenefitsListOffers,
  EmailPopupStrings.BenefitsListOffersFreeItems,
];

export const EmailPopup: React.FC = () => {
  const history = useHistory();

  const [isEmailPopup, setIsEmailPopup] = useState(true);
  const [email, setEmail] = useState('');

  useEffect(() => {
    mainWrapperScrollManager('hidden');
  }, []);

  const onChangeEmailHandler = (email: string) => {
    setEmail(email);
  };

  const onSubmitEmailHandler = () => {};

  const onCloseEmailPopupHandler = () => {
    setIsEmailPopup(false);
    mainWrapperScrollManager('auto');
  };

  const BenefitsListBlock = useMemo(() => {
    return (
      <BenefitsList>
        {benefits.map((benefitItem) => (
          <BenefitsItem key={benefitItem}>{benefitItem}</BenefitsItem>
        ))}
      </BenefitsList>
    );
  }, []);

  if (!isEmailPopup) {
    return null;
  }

  const redirectOnPrivacyPolicyHandler = () => {
    onCloseEmailPopupHandler();
    history.push(AppRoutes.PrivacyPolicy);
  };

  return (
    <Modal onClose={onCloseEmailPopupHandler}>
      <ModalContent>
        <TopImage src={TopImageMock} />
        <PromoteText>
          <Title
            firstText={EmailPopupStrings.TitleFirstText}
            secondaryText={EmailPopupStrings.TitleSecondText}
            noPadding
          />
          {BenefitsListBlock}

          <InputWithButtonField
            buttonText="Join"
            value={email}
            placeholder="Enter your email"
            onChange={onChangeEmailHandler}
            onSubmit={onSubmitEmailHandler}
          />
          <PrivacyPolicy onClick={redirectOnPrivacyPolicyHandler}>
            {EmailPopupStrings.PrivacyPolicyText1} <span>{EmailPopupStrings.PrivacyPolicyText2}</span>
          </PrivacyPolicy>
        </PromoteText>
      </ModalContent>
    </Modal>
  );
};
