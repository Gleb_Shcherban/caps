import styled from 'styled-components';

export const ModalContent = styled.div`
  background-color: #252525;
`;

export const TopImage = styled.img``;

export const PromoteText = styled.div`
  padding: 15px;
`;

export const BenefitsList = styled.ul`
  margin: 10px 0 30px 0;
`;

export const BenefitsItem = styled.li`
  margin-bottom: 5px;
  font-size: 18px;
`;

export const PrivacyPolicy = styled.div`
  margin-top: 10px;
  text-align: center;
  font-weight: 300;
  font-size: 10px;
  color: var(--greyTextColor);

  span {
    color: #d0d0d0;
  }
`;
