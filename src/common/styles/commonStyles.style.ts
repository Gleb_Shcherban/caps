import styled from 'styled-components';

export const Button = styled.button`
  width: 100%;
  height: 58px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: var(--primaryBgrGradientVertical);
  font-size: 14px;
  font-weight: bold;
  border-radius: var(--buttonBorderRadius);
`;

export const SectionDivider = styled.div`
  border-top: 1px solid var(--dividerColor);
  margin: 20px -10px 18px -10px;
  opacity: 0.3;
`;
