export interface StoryVideosApiModel {
  id: string;
  uri: string;
  cloudinaryUrl: string;
  venueId: string;
  userId: string;
  shortcode: string;
  stockAudioId: string | null;
  stockVideoId: string | null;
  createdAt: string;
  campaignId?: string | null;
}