import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { AppRoutes } from './common/constants/routes';

import { HomePage } from './pages/HomePage/HomePage';
import { UploadVideo } from './pages/UploadVideo/UploadVideo';
import { StoryPage } from './pages/StoryPage/StoryPage';
import { PrivacyPolicyPage } from './pages/PrivacyPolicyPage/PrivacyPolicyPage';
import { EmailPopup } from './common/components/EmailPopup/EmailPopup';

import './reset.css';
import { AppContainer } from './App.style';
// import { Sidebar } from './common/components/SideBar/SideBar';
import { ReferencesPage } from './pages/References/References';

function App() {
  return (
    <AppContainer className="App">
      <Router>
        <EmailPopup />
        <Switch>
          <Route exact path={AppRoutes.Home}>
            <HomePage />
          </Route>
          <Route path={AppRoutes.UploadVideo}>
            <UploadVideo />
          </Route>
          <Route path={AppRoutes.Story}>
            <StoryPage />
          </Route>
          <Route path={AppRoutes.References}>
            <ReferencesPage />
          </Route>
          <Route path={AppRoutes.PrivacyPolicy}>
            <PrivacyPolicyPage />
          </Route>
        </Switch>
      </Router>
    </AppContainer>
  );
}

export default App;
