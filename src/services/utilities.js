export const mainWrapperScrollManager = (property = 'auto') => {
  const commonWrapper = document.body.querySelector('.App');
  commonWrapper.style.overflow = property;
};