import React from 'react';

import { InputWithButtonField } from '../../common/components/InputWithButtonField/InputWithButtonField';
import { EmailPopup } from '../../common/components/EmailPopup/EmailPopup';

import { HomePageContainer } from './HomePage.style';

export const HomePage: React.FC = () => {
  return (
    <HomePageContainer>
      <InputWithButtonField
        buttonText="Join"
        value=""
        placeholder="Enter your email"
        onChange={() => {}}
        onSubmit={() => {}}
      />
      <EmailPopup />
    </HomePageContainer>
  );
};
