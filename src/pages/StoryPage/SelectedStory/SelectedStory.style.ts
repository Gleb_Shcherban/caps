import styled from 'styled-components';
import { Button } from '../../../common/styles/commonStyles.style';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const StoryInfo = styled.div`
  display: flex;
  align-items: center;
`;

export const StoryImageWrapper = styled.div`
  margin-right: 10px;
  width: 36px;
  height: 36px;
  border: 3px solid var(--blue);
  border-radius: 50%;
`;

export const StoryImage = styled.img`
  border-radius: 50%;
`;

export const StoryName = styled.div`
  font-size: 16px;
  font-weight: 800;
`;

export const ChangeStoryButton = styled(Button)`
  width: 104px;
  height: 25px;
  font-size: 10px;
  font-weight: 500;
  border-radius: 20px;
`;
