import React from 'react';
import { useHistory } from 'react-router-dom';

import { AppRoutes } from '../../../common/constants/routes';

import { StoryPageStrings } from '../../../common/localization/en';

import CapImage from './Cap.jpg';

import {
  Container,
  StoryImage,
  StoryImageWrapper,
  StoryName,
  ChangeStoryButton,
  StoryInfo,
} from './SelectedStory.style';

interface SelectedStoryProps {
  img: string;
  name: string;
}

const mockData: SelectedStoryProps = {
  img: CapImage,
  name: 'A-Game Hydros',
};

export const SelectedStory: React.FC = () => {
  const history = useHistory();

  const redirectOnStoriesHandler = () => {
    history.push(AppRoutes.References);
  };

  return (
    <Container>
      <StoryInfo>
        <StoryImageWrapper>
          <StoryImage src={mockData.img} />
        </StoryImageWrapper>
        <StoryName>{mockData.name}</StoryName>
      </StoryInfo>
      <ChangeStoryButton onClick={redirectOnStoriesHandler}>{StoryPageStrings.ChangeStory}</ChangeStoryButton>
    </Container>
  );
};
