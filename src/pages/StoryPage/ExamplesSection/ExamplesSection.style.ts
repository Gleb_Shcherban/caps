import styled from 'styled-components';

export const Container = styled.div`
  padding: var(--pagePadding) 0 var(--pagePadding) var(--pagePadding);
`;

export const Title = styled.div`
  margin: 18px 0 10px;
  font-size: 18px;
  font-weight: 800;
`;

export const Videos = styled.div`
  margin: 0 10px 30px 0;
`;

export const VideosRow = styled.div`
  display: flex;
  width: 100%;
  overflow-x: scroll;
`;
