import { StoryVideosApiModel } from '../../../../api/models/videos';

export const videosMockData: StoryVideosApiModel[] = [
  {
    id: 'e0d3c23d-bf6d-4eae-8db9-3e95f38922c3',
    uri: 's3://rockets-sv-user-content/stories/4a27df9e-8a2c-4f30-af4f-5304fbc486dd.mp4',
    cloudinaryUrl:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,du_10,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-yevhh/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:ff22edef-61b5-461b-b8d1-b622ee196097-201afc66-7bde-4607-a870-beb09e16aa6f,so_10,w_374/fl_layer_apply/ac_aac,c_fill,du_10,eo_10,g_north,h_200,l_video:cimd5lu5xtpojzstxis4,so_0,w_280/e_fade:-1500/v1/fanstory-amp/video/f7702eb9-b7f2-4807-8676-f9e1f08e81db.mp4',
    venueId: 'db373848-04cf-4f76-9750-e82e78b1decc',
    userId: 'ff22edef-61b5-461b-b8d1-b622ee196097',
    shortcode: 'mfclEkP2',
    stockAudioId: null,
    stockVideoId: '201afc66-7bde-4607-a870-beb09e16aa6f',
    createdAt: '2021-02-22T06:53:43.146058Z',
  },
  {
    id: '86b2604e-f949-43c1-ad20-8b953d94f9e9',
    uri: 's3://rockets-sv-user-content/stories/97fe3ff5-7d0c-4520-ba0e-025bfc01d1c6.mp4',
    cloudinaryUrl:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-nvm53/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:df03140c-b8b1-408f-b50d-4ded9b8a96bd-9fb02a32-9d4a-46e5-96bf-5f3503468266,w_374/fl_layer_apply/ac_aac,c_fill,e_loop:10,eo_31.43400001525879,g_north,h_200,l_video:ikjejydne7spohwjdvtn,r_0,so_0,w_280/v1/fanstory-amp/videos/69fb0b8d-2ed3-4123-995a-74fdf7be7666.mp4',
    venueId: 'db373848-04cf-4f76-9750-e82e78b1decc',
    userId: 'df03140c-b8b1-408f-b50d-4ded9b8a96bd',
    shortcode: 'ngphbYkY',
    stockAudioId: null,
    stockVideoId: '9fb02a32-9d4a-46e5-96bf-5f3503468266',
    createdAt: '2021-02-16T15:43:20.698800Z',
  },
  {
    id: 'edfb0d4f-ed07-408b-bdd7-1d1244bf5a3a',
    uri: 's3://rockets-sv-user-content/stories/ee804581-8b9c-4320-b1a3-c2387aeb0b92.mp4',
    cloudinaryUrl:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-nvm53/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:df03140c-b8b1-408f-b50d-4ded9b8a96bd-9fb02a32-9d4a-46e5-96bf-5f3503468266,w_374/fl_layer_apply/ac_aac,c_fill,e_loop:10,eo_31.43400001525879,g_north,h_200,l_video:il9jcaiebyjekkhwsnux,r_0,so_0,w_280/v1/fanstory-amp/videos/69fb0b8d-2ed3-4123-995a-74fdf7be7666.mp4',
    venueId: 'db373848-04cf-4f76-9750-e82e78b1decc',
    userId: 'df03140c-b8b1-408f-b50d-4ded9b8a96bd',
    shortcode: 'GKQPLfJC',
    stockAudioId: null,
    stockVideoId: '9fb02a32-9d4a-46e5-96bf-5f3503468266',
    createdAt: '2021-02-16T15:06:34.209718Z',
  },
  {
    id: '6dbebb37-c22e-4912-9818-002cab6d1cba',
    uri: 's3://rockets-sv-user-content/stories/16166fd8-3113-4840-83ca-a19816588961.mp4',
    cloudinaryUrl:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-nvm53/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:df03140c-b8b1-408f-b50d-4ded9b8a96bd-ae5cbc16-38ca-406b-9ab8-306424c3a10a,w_374/fl_layer_apply/ac_aac,c_fill,e_loop:10,eo_15.133999824523926,g_north,h_200,l_video:yzmyookjk1bcucskbxnw,r_0,so_0,w_280/v1/fanstory-amp/videos/ab504867-33d9-43d5-8f7e-f304ad657288.mp4',
    venueId: 'db373848-04cf-4f76-9750-e82e78b1decc',
    userId: 'df03140c-b8b1-408f-b50d-4ded9b8a96bd',
    shortcode: 'LLzdPbj2',
    stockAudioId: null,
    stockVideoId: 'ae5cbc16-38ca-406b-9ab8-306424c3a10a',
    createdAt: '2021-02-16T14:58:56.441690Z',
  },
];
