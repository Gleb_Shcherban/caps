import React from 'react';

import { VideoComponent } from '../../../common/components/VideoComponent/VideoComponent';

import { videosMockData } from './mockData/videos';

import { VideoExampleWrapper } from '../../../common/components/VideoComponent/VideoComponent.style';
import { Container, Title, Videos, VideosRow } from './ExamplesSection.style';

export const ExamplesSection: React.FC = () => {
  const videoExamples = videosMockData;

  return (
    <Container>
      <Title>Examples</Title>
      <Videos>
        <VideosRow>
          {videoExamples.map((videoExample) => (
            <VideoExampleWrapper>
              <VideoComponent url={videoExample.cloudinaryUrl} />
            </VideoExampleWrapper>
          ))}
        </VideosRow>
      </Videos>
    </Container>
  );
};
