import styled from 'styled-components';

export const Container = styled.div`
  padding: var(--pagePadding);
`;

export const ExplanationAboutContent = styled.div`
  margin-top: 10px;
  text-align: center;
  font-weight: 300;
  font-size: 10px;
  color: var(--greyTextColor);
`;
