import React from 'react';

import { StoryPageStrings } from '../../common/localization/en';

import { TopBar } from '../../common/components/TopBar/TopBar';
import { Title } from '../../common/components/Title/Title';
import { SelectedStory } from './SelectedStory/SelectedStory';
import { SliderSection } from './SliderSection/SliderSection';
import { UploadButtonComponent } from './UploadButtonComponent/UploadButtonComponent';
import { ExamplesSection } from './ExamplesSection/ExamplesSection';

import { SectionDivider } from '../../common/styles/commonStyles.style';
import { Container, ExplanationAboutContent } from './StoryPage.style';

export const StoryPage: React.FC = () => {
  const onChangeUploadVideoHandler = (file: File) => {};

  return (
    <>
      <Container>
        <TopBar />
        <Title firstText={StoryPageStrings.TitleFirstText} secondaryText={StoryPageStrings.TitleSecondText} />
        <SectionDivider />
        <SelectedStory />
        <SliderSection />
        <UploadButtonComponent onChange={onChangeUploadVideoHandler} />
        <ExplanationAboutContent>{StoryPageStrings.ExplanationAboutContent}</ExplanationAboutContent>
      </Container>
      <ExamplesSection />
    </>
  );
};
