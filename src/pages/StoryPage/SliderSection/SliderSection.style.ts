import styled from 'styled-components';

export const Slide = styled.div`
  position: relative;
  width: 100%;
  height: 300px;
  object-position: center;
  object-fit: cover;
  flex-shrink: 0;
  outline: none;

  > img {
    height: 190px;
  }
`;

export const WrapperLinkToAsButton = styled.div`
  width: 247px;
  margin: 0 auto 11px;
`;

export const SliderContent = styled.div`
  padding: var(--pagePadding);
`;

export const SliderWrapper = styled.div`
  margin: 10px 0;
  width: 100%;
  height: 300px;
  border-radius: var(--commonBorderRadius);
  overflow: hidden;
  background-color: var(--darkGreyBg);

  .slick-dots {
    width: fit-content;
    bottom: 15px;
    left: 0;
    right: 0;
    margin: auto;
  }

  .slick-dots li.slick-active button:before {
    opacity: 1;
    color: var(--white);
  }

  .slick-dots li button:before {
    opacity: 0.2;
    color: var(--white);
  }

  .slick-dots li {
    width: 6px;
    height: 6px;
    margin: 0 14px 0 0;
  }

  .slick-dots li button {
    width: 6px;
    height: 6px;
  }
`;

export const SliderTitleSection = styled.div`
  display: flex;
  justify-content: space-between;

  img {
    width: 17px;
    height: 17px;
  }
`;

export const SliderTitle = styled.h2`
  margin-bottom: 6px;
  font-size: 16px;
  font-weight: 800;

  span {
    color: var(--accentTextColor);
  }
`;

export const SliderText = styled.p`
  font-size: 12px;
  font-weight: 300;
`;
