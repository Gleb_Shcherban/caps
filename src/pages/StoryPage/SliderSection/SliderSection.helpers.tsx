import React from 'react';
import { NextArrow } from './Arrows/Arrows';

import mockImage from './Bitmap.png';

export const sliderSettings = {
  dots: true,
  arrows: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 5000,
  pauseOnHover: true,
  pauseOnFocus: true,
  cssEase: 'linear',
  nextArrow: <NextArrow />,
};

interface SliderSectionProps {
  id: string;
  isVideo: boolean;
  url: string;
  title: string;
  text: string;
}

export const mockSliders: SliderSectionProps[] = [
  {
    id: '1',
    isVideo: true,
    url:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,du_10,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-yevhh/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:ff22edef-61b5-461b-b8d1-b622ee196097-201afc66-7bde-4607-a870-beb09e16aa6f,so_10,w_374/fl_layer_apply/ac_aac,c_fill,du_10,eo_10,g_north,h_200,l_video:cimd5lu5xtpojzstxis4,so_0,w_280/e_fade:-1500/v1/fanstory-amp/video/f7702eb9-b7f2-4807-8676-f9e1f08e81db.mp4',
    title: 'Tip',
    text: 'Tell us about yourself and show what you do when you’re wearing your Melin hat.',
  },
  {
    id: '2',
    isVideo: false,
    url: mockImage,
    title: 'Tip',
    text: 'Tell us about yourself and show what you do when you’re wearing your Melin hat.',
  },
  {
    id: '3',
    isVideo: true,
    url:
      'http://res.cloudinary.com/resistr/video/upload/ac_aac,c_fill,g_north,h_552,w_374/co_rgb:FFAD04,l_text:Arial_16_bold:sv-nvm53/fl_layer_apply,x_255,y_215/c_fill,du_3,fl_splice,h_552,l_fanstory-amp:endcard:df03140c-b8b1-408f-b50d-4ded9b8a96bd-9fb02a32-9d4a-46e5-96bf-5f3503468266,w_374/fl_layer_apply/ac_aac,c_fill,e_loop:10,eo_31.43400001525879,g_north,h_200,l_video:ikjejydne7spohwjdvtn,r_0,so_0,w_280/v1/fanstory-amp/videos/69fb0b8d-2ed3-4123-995a-74fdf7be7666.mp4',
    title: 'Tip',
    text: 'Tell us about yourself and show what you do when you’re wearing your Melin hat.',
  },
];
