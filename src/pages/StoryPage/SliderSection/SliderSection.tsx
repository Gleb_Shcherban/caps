import React, { useCallback } from 'react';
import Slider from 'react-slick';

import { VideoComponent } from '../../../common/components/VideoComponent/VideoComponent';

import { sliderSettings, mockSliders } from './SliderSection.helpers';

import StickImage from '../../../common/assets/images/StickImage.png';

import { VideoSliderWrapper } from '../../../common/components/VideoComponent/VideoComponent.style';
import { SliderContent, SliderWrapper, Slide, SliderTitleSection, SliderTitle, SliderText } from './SliderSection.style';

export const SliderSection: React.FC = () => {
  const renderMediaBlock = useCallback((item) => {
    if (!item.isVideo) {
      return <img src={item.url} alt="story" />;
    }

    return (
      <VideoSliderWrapper>
        <VideoComponent url={item.url} />
      </VideoSliderWrapper>
    );
  }, []);

  const renderSlides = () => {
    return mockSliders.map((item, index) => {
      const sliderNumber = index + 1;
      return (
        <Slide key={item.id}>
          {renderMediaBlock(item)}
          <SliderContent>
            <SliderTitleSection>
              <SliderTitle>
                {item.title} <span>#{sliderNumber}</span>
              </SliderTitle>
              <img src={StickImage} alt="stick" />
            </SliderTitleSection>
            <SliderText>{item.text}</SliderText>
          </SliderContent>
        </Slide>
      );
    });
  };

  return (
    <SliderWrapper>
      <Slider {...sliderSettings}>{renderSlides()}</Slider>
    </SliderWrapper>
  );
};
