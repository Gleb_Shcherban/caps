import React from 'react';

import { ArrowButton } from './Arrows.style';

interface NextArrowProps {
  onClick?: () => void;
}

export const NextArrow: React.FC<NextArrowProps> = (props) => {
  const { onClick } = props;
  return (
    <ArrowButton onClick={onClick} aria-label="Next">
      next
    </ArrowButton>
  );
};
