import styled from 'styled-components';

export const ArrowButton = styled.div`
  bottom: 10px;
  right: 10px;
  position: absolute;
  font-size: 14px;
`;