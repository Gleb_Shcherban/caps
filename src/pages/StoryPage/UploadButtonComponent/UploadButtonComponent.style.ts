import styled from 'styled-components';
import { Button } from '../../../common/styles/commonStyles.style';

export const Container = styled.div``;

export const UploadInput = styled.input`
  display: none;
`;

export const UploadButton = styled(Button)`
  pointer-events: none;

  img {
    margin-right: 10px;
    width: 26px;
    height: 17px;
  }
`;
