import React from 'react';

import { StoryPageStrings } from '../../../common/localization/en';

import MediaPicture from '../../../common/assets/images/MediaPicture.png';

import { Container, UploadButton, UploadInput } from './UploadButtonComponent.style';

interface UploadButtonComponentProps {
  onChange: (file: File) => void;
}

export const UploadButtonComponent: React.FC<UploadButtonComponentProps> = ({ onChange }) => {
  const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files ? event.target.files[0] : null;
    if (!file) {
      return;
    }

    onChange(file);
  };

  return (
    <Container>
      <label htmlFor="upload-input">
        <UploadButton>
          <img src={MediaPicture} alt="media" />
          {StoryPageStrings.UploadButton}
        </UploadButton>
        <UploadInput id="upload-input" name="upload-input" type="file" accept="video/*" onChange={onChangeHandler} />
      </label>
    </Container>
  );
};
