import styled from 'styled-components';

export const Container = styled.div`
  padding: var(--pagePadding);
  text-align: center;
`;
