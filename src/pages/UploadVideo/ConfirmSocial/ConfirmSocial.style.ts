import styled from 'styled-components';

export const ConfirmSocialWrapper = styled.div`
  background-color: var(--darkGreyBg);
  display: flex;
  border-radius: 10px;
  min-height: 348px;
  overflow: hidden;
  flex-direction: column;
`;

interface ProductImageProps {
  url: string;
}

export const ProductImage = styled.div<ProductImageProps>`
  flex: 1;
  background-color: var(--white);
  background-size: cover;
  background-position: center;
  ${(props) => `
    background-image: url(${props.url});
  `}
`;
