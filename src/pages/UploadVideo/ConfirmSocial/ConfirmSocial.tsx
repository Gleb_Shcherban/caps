import React from 'react';
import { ConfirmSocialWrapper, ProductImage } from './ConfirmSocial.style';

import HatsImage from '../../../common/assets/images/hats.jpg';

export const ConfirmSocial: React.FC = () => {
  return (
    <ConfirmSocialWrapper>
      <ProductImage url={HatsImage} />
      <div style={{ flex: 1, boxSizing: 'border-box' }}>
        <p></p>
      </div>
    </ConfirmSocialWrapper>
  );
};
