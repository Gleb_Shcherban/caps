import styled from 'styled-components';

export const ConfirmEmailWrapper = styled.div`
  border-radius: 8px;
  background-color: var(--darkGreyBg);
  padding: 27px 10px 20px 10px;
  text-align: center;
  position: relative;
  margin-bottom: 14px;
`;

export const ConfirmBadge = styled.span`
  position: absolute;
  font-size: 10px;
  font-weight: 400;
  right: 8px;
  top: 10px;
  color: var(--accentTextColor);
`;

export const ConfirmTitle = styled.div`
  font-size: 14px;
  font-weight: 400;
  margin-bottom: 22px;
`;

export const ConfirmedTitle = styled.div`
  color: var(--darkGreyTextColor);
  font-weight: 800;
  font-size: 18px;
  margin-bottom: 24px;
`;

export const TextDescription = styled.div`
  font-size: 14px;
  font-weight: 300;
`;
