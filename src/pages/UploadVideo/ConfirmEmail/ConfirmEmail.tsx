import React from 'react';
import { InputWithButtonField } from '../../../common/components/InputWithButtonField/InputWithButtonField';
import { UploaVideoStrings } from '../../../common/localization/en';
import {
  // ConfirmBadge,
  // ConfirmedTitle,
  ConfirmEmailWrapper,
  ConfirmTitle,
  // TextDescription,
} from './ConfirmEmail.style';
// import { EmailIcon } from '../../../common/assets/EmailIcon';

export const ConfirmEmail: React.FC = () => {
  return (
    <ConfirmEmailWrapper>
      <ConfirmTitle>{UploaVideoStrings.ConfirmEmail}</ConfirmTitle>
      <InputWithButtonField
        onChange={() => {}}
        onSubmit={() => {}}
        buttonText="Confirm"
        value=""
        placeholder="Your email"
      />
      {/* <ConfirmBadge>Confirmed</ConfirmBadge>
      <div>
        <EmailIcon  />
        <ConfirmedTitle>akb@codemotion.eu</ConfirmedTitle>
        <TextDescription>We’ll review and then email your reward. </TextDescription>
      </div> */}
    </ConfirmEmailWrapper>
  );
};
