import React, { useState } from 'react';
import { Progress } from '../../common/components/Progress/Progress';
import { ConfirmEmail } from './ConfirmEmail/ConfirmEmail';
import { ConfirmSocial } from './ConfirmSocial/ConfirmSocial';
import { MainText, UploadingHeader, UploadVideoContainer } from './UploadVideo.style';
import { SectionDivider } from '../../common/styles/commonStyles.style';
import { TopBar } from '../../common/components/TopBar/TopBar';

export const UploadVideo: React.FC = () => {
  const [progress] = useState(0);

  return (
    <UploadVideoContainer>
      <TopBar />
      <UploadingHeader>
        <MainText>Uploading</MainText>
        <MainText highlight>75%</MainText>
      </UploadingHeader>
      <Progress now={progress} />
      <SectionDivider />
      <ConfirmEmail />
      <ConfirmSocial />
    </UploadVideoContainer>
  );
};
