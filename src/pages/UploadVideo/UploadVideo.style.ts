import styled from 'styled-components';

export const UploadVideoContainer = styled.div`
  max-width: 600px;
  margin: 0 auto;
  padding: var(--pagePadding);
  box-sizing: border-box;
`;

export const UploadingHeader = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  padding-bottom: 26px;
`;

interface MainTextProps {
  highlight?: boolean;
}
export const MainText = styled.div<MainTextProps>`
  font-size: 28px;
  color: var(--white);
  font-weight: bold;
  ${(props) => (props.highlight ? `color: var(--accentTextColor)` : ``)}
`;
