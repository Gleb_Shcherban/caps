import image1 from './1.png';
import image2 from './2.png';
import image3 from './3.png';
import image4 from './4.png';
import image5 from './5.png';

export interface StoryMockData {
  id: string;
  title: string;
  imageUrl: string;
}

export const storiesMockData: StoryMockData[] = [
  {
    id: '1',
    title: 'A-Game Hydros',
    imageUrl: image1,
  },
  {
    id: '2',
    title: 'Trench Icon Hydros',
    imageUrl: image2,
  },
  {
    id: '3',
    title: 'Odyssey Hydros',
    imageUrl: image3,
  },
  {
    id: '4',
    title: 'A-Day Beanies',
    imageUrl: image4,
  },
  {
    id: '5',
    title: 'Sustainable Manufacturing',
    imageUrl: image5,
  },
];
