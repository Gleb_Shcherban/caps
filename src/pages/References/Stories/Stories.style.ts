import styled from 'styled-components';

export const StoriesContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StoryCard = styled.div`
  display: flex;
  height: 85px;
  width: 100%;
  border-radius: var(--buttonBorderRadius);
  background-color: var(--darkGreyBg);
  margin-bottom: 10px;
`;

export const StoryCardImage = styled.img`
  width: 138px;
  height: auto;
  border-radius: var(--commonBorderRadius) 0 0 var(--commonBorderRadius);
`;

export const StoryCardText = styled.div`
  width: 100%;
  height: 100%;
  padding: 13px 20px;
  font-size: 16px;
  font-weight: 300;
`;
