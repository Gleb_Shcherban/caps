import React from 'react';
import { StoriesContainer, StoryCard, StoryCardImage, StoryCardText } from './Stories.style';
import { storiesMockData } from './mockData/storiesMockData';

export const Stories: React.FC = () => {
  // TODO: get stories from some sort of store
  const stories = storiesMockData;

  const onStoryClick = (id: string) => {
    console.log('story clicked', id);
  };

  return (
    <StoriesContainer>
      {stories.map((storyItem) => (
        <StoryCard key={storyItem.id} onClick={() => onStoryClick(storyItem.id)}>
          <StoryCardImage src={storyItem.imageUrl} alt="" />
          <StoryCardText>{storyItem.title}</StoryCardText>
        </StoryCard>
      ))}
    </StoriesContainer>
  );
};
