import styled from 'styled-components';

export const ReferencesPageContainer = styled.div`
  height: 100%;
  width: 100%;
  padding: var(--pagePadding);
`;
