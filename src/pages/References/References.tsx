import React from 'react';
import { TopBar } from '../../common/components/TopBar/TopBar';
import { Title } from '../../common/components/Title/Title';
import { Stories } from './Stories/Stories';
import { ReferencesPageContainer } from './References.style';
import { SectionDivider } from '../../common/styles/commonStyles.style';
import { ReferencesPageStrings } from '../../common/localization/en';

export const ReferencesPage: React.FC = () => {
  return (
    <ReferencesPageContainer>
      <TopBar />
      <Title firstText={ReferencesPageStrings.TitleFirstText} secondaryText={ReferencesPageStrings.TitleSecondText} />
      <SectionDivider />
      <Stories />
    </ReferencesPageContainer>
  );
};
